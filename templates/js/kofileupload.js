//required : jquery.js and knockout.js

//knockoutjs start
function AppViewModel() {
    var self = this;
 
    self.imgurls = ko.observableArray([

    ]);
 
    self.removePerson = function() {
        self.imgurls.remove(this);
    }
}

var appvm_fileupload = new AppViewModel()
ko.applyBindings(appvm_fileupload, document.getElementById("imgurls"));
//knockoutjs end

// Variable to store your files
var files;

// Add events
$('input[type=file]').on('change', postToImgur);

function postToImgur(event) {
  files = event.target.files;
  var formData = new FormData();
  if(files[0] =="") return;
  formData.append("image", files[0]);
  $.ajax({
    url: "https://api.imgur.com/3/image",
    type: "POST",
    datatype: "json",
    headers: {
      "Authorization": "Client-ID 2a3622b93ba8372"
    },
    data: formData,
    success: function(response) {
      var photourl = response.data.link;
      var width = response.data.width;
      var height = response.data.height;

      var photo_hash = response.data.deletehash;
      console.log(photourl + " " + photo_hash);
	    document.getElementById("fileapath").value = "";
      appvm_fileupload.imgurls.push({ url: photourl + "#" + width + "#" + height});
      $('.meter>span').css('width', 0+'%');  

    },
    cache: false,
    contentType: false,
    processData: false,
	  xhr: function() {
					var xhr = new window.XMLHttpRequest();

				    //Upload progress
				    xhr.upload.addEventListener("progress", function(evt){
				      if (evt.lengthComputable) {  
				        var percentComplete = evt.loaded*100 / evt.total;
				        console.log(evt.loaded+ " " +percentComplete);
				        //Do something with upload progress
                $('.meter>span').css('width', percentComplete+'%');  

				      }
				    }, false);
				    return xhr;
				},
    });
}
