var CreeatPhotoSwipe_Gallery = function(stritem,row) {
    $( "#pswp").unbind( "PhotoSwipe.EventTypes.onHide" );
    arritem = stritem.split(",");
    var items = [];
    arritem.forEach(function(item){
      parseditem = item.split("#");
      var url = parseditem[0];
      var w = parseInt(parseditem[1]);
      var h = parseInt(parseditem[2]);
      //console.log("owner:"+ row.Owner);
      if(url!="") {
        //console.log("url:" + url +" w:" + w + " h:" + h);
        if ((isNaN(w) === false) && (isNaN(h) === false)) {
          items.push({src:url,w:w,h:h,title:row.Nickname});
        } else {
          items.push({src:url,w:720,h:720,title:row.Nickname});
        }
      }
    });
    if(items.length == 0)
      return null;
    var pswpElement = document.querySelectorAll('.pswp')[0];
    // define options (if needed)
    var options = {
       // history & focus options are disabled on CodePen        
        //history: false,
        //focus: false,
        //preventHide:true,
        //captionAndToolbarHide:true,
        tapToToggleControls: false,
        captionAndToolbarHideOnSwipe:false,
        captionAndToolbarAutoHideDelay:0,
        showAnimationDuration: 0,
        hideAnimationDuration: 0,
        //captionAndToolbarShowEmptyCaptions: false,
    };
    return new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    //this.gallery.init();
};

