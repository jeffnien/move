$(window).resize(function () {
  var h = $(window).height(),
    offsetTop = 105; // Calculate the top offset

  //$('#map').css('height', (h - offsetTop));
  $('#map_from').css('height', "300px");
}).resize();
$(window).resize(function () {
  var h = $(window).height(),
    offsetTop = 105; // Calculate the top offset

  //$('#map').css('height', (h - offsetTop));
  $('#map_to').css('height', "300px");
}).resize();
// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.


var gmap_from = null;
var gmap_to = null;

function initMap() {
  var map_from = new google.maps.Map(document.getElementById('map_from'), {
    center: {lat: 25.001689, lng: 121.460809},//taipei city
    //center: {lat: -33.9, lng: 151.2},
    zoom: 15,
    minZoom: 13, // remember not to get data when zoom 13
    streetViewControl: false,
    disableDefaultUI: true,
    fullscreenControl: true,
    zoomControl: true
  });
  map_from.myOwnerMapMarker = {};
  map_from.infoWindow = new google.maps.InfoWindow({map: map_from});
  // Create the DIV to hold the control and call the CenterControl() constructor
  // passing in this DIV.
  var centerControlDiv = document.createElement('div');
  var centerControl = new CenterControl(centerControlDiv);
  centerControlDiv.index = 1;
  map_from.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
  TriggerCurrentPostionByClick();

  var map_to = new google.maps.Map(document.getElementById('map_to'), {
    center: {lat: 25.001689, lng: 121.460809},//taipei city
    //center: {lat: -33.9, lng: 151.2},
    zoom: 15,
    minZoom: 13, // remember not to get data when zoom 13
    streetViewControl: false,
    disableDefaultUI: true,
    fullscreenControl: true,
    zoomControl: true
  });
  map_to.myOwnerMapMarker = {};



  gmap_from = map_from;
  gmap_to = map_to;


}
function TriggerCurrentPostionByClick() {
  if(gmap_from == null) return;
  var map = gmap_from;

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      if (typeof map.circle == 'undefined'){
        map.circle = new google.maps.Circle({
          center: pos,
          radius: 100,
          fillColor: "#0000FF",
          fillOpacity: 0.1,
          map: map,
          strokeColor: "#FFFFFF",
          strokeOpacity: 0.1,
          strokeWeight: 2
        });
      }
      if (typeof map.myloc == 'undefined'){
        map.myloc = new google.maps.Marker({
            position: pos,
            clickable: false,//mylocation-sprite-cookieless-v2-1x.png
            icon: new google.maps.MarkerImage('http://maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
                                                            new google.maps.Size(22,22),
                                                            new google.maps.Point(0,18),
                                                            new google.maps.Point(11,11)),
            shadow: null,
            zIndex: 999,
            map: map// your google.maps.Map object
        });
      }
      map.infoWindow.setPosition(pos);
      map.infoWindow.setContent('您在這裡.');
      map.infoWindow.open(map);
      //map.infoWindow.close(map);
      map.setCenter(pos);
      setTimeout(setCurrentPostionByTimerHandler, 1000);

    }, function() {
      handleLocationError(true, map.infoWindow, map.getCenter());
      setTimeout(setCurrentPostionByTimerHandler, 1000);
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, map.infoWindow, map.getCenter());
    setTimeout(setCurrentPostionByTimerHandler, 1000);
  }
}

var last_current_pos = {lat: 25.001689, lng: 121.460809};
function setLastCurrentPostionByClick() {
  if(gmap_from == null) return;
  var map = gmap_from;
  map.infoWindow.setPosition(last_current_pos);
  //map.infoWindow.setContent('您在這裡.');
  map.infoWindow.open(map);
  //map.infoWindow.close(map);
  map.setCenter(last_current_pos);
}

function setCurrentPostionByTimerHandler() {
  //console.log("setCurrentPostionByTimer invoked.");
  var map = gmap_from;
  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
//      console.log(pos);
      last_current_pos.lat = pos.lat;
      last_current_pos.lng = pos.lng;
      map.circle.setCenter(pos);
      map.myloc.setPosition(pos);
      setTimeout(setCurrentPostionByTimerHandler, 1000);
    }, function() {
      handleLocationError(true, map.infoWindow, map.getCenter());
      setTimeout(setCurrentPostionByTimerHandler, 1000);
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, map.infoWindow, map.getCenter());
    setTimeout(setCurrentPostionByTimerHandler, 1000);
  }
  
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}
function setMarker(map, seller, lat, lng, content) {
  if(map == null) return;
  var marker;
  marker = map.myOwnerMapMarker[seller.Owner];
  //console.log(marker);
  if(typeof marker != "undefined") {
    return ;
  }
  // Adds markers to the map.

  // Marker sizes are expressed as a Size of X,Y where the origin of the image
  // (0,0) is located in the top left of the image.

  // Origins, anchor positions and coordinates of the marker increase in the X
  // direction to the right and in the Y direction down.
  var image = {
    url: '../img/beachflag.png',
    // This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(20, 32),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(0, 32)
  };
  // Shapes define the clickable region of the icon. The type defines an HTML
  // <area> element 'poly' which traces out a polygon as a series of X,Y points.
  // The final coordinate closes the poly by connecting to the first coordinate.
  var shape = {
    coords: [1, 1, 1, 20, 18, 20, 18, 1],
    type: 'poly'
  };

    var pos = {lat: lat, lng: lng};
    marker = new google.maps.Marker({
      position: pos,
      map: map,
      icon: image,
//      shape: shape,
      title: seller.Username,
 //     zIndex: beach[3]
    });
    marker.pos = pos;
/*
                    "<p>" + seller.Username +
                    "(LINE：" + seller.Line +
                    ",TEL：" + seller.Telephone + ")" +
                    //",Email：" + seller.Email +
                    "<p>地址：" + seller.City + seller.District + seller.Roadname +
                    "<p>服務項目：" + seller.Checkstrs +
                    "<p>備註：" + seller.Content;
*/
    marker.infowindow = new google.maps.InfoWindow({content:content});

    map.premarker = marker;

    function marker_click_callback() {
      map.premarker.infowindow.close();
      this.infowindow.open(map, this);
      map.premarker = this;
      map.setCenter(this.pos);

    }
    //map.addListener('dragend', function() {premarker.infowindow.close();});
    marker.addListener('click', marker_click_callback, false);
    map.myOwnerMapMarker[seller.Owner] = marker;
}

function trigger_marker_click(map, owner) {
  var marker = map.myOwnerMapMarker[owner];
  //console.log("QQQQAAAAAA:" + marker.ddd);
  //marker.dispatchEvent(new Event('click'))
  google.maps.event.trigger(marker, 'click');
  //marker.trigger("click");
}
/**
 * The CenterControl adds a control to the map that recenters the map on Chicago.
 * This constructor takes the control DIV as an argument.
 * @constructor
 */
function CenterControl(controlDiv) {

  // Set CSS for the control interior.
  var controlButton= document.createElement('button');
  controlButton.className="btn btn-warning";
  controlButton.innerHTML = '我在哪呢？';

  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.appendChild(controlButton);
  
  controlDiv.appendChild(controlUI);

  // Setup the click event listeners: simply set the map to Chicago.
  controlUI.addEventListener('click', function() {
    setLastCurrentPostionByClick();
  });
}

