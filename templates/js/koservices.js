var myservices = [
{"service":"兼職搬家","checked": false},
{"service":"自助搬家","checked": false},
{"service":"套房搬家","checked": false},
{"service":"小型搬家","checked": false},
{"service":"有搬物服務","checked": false},
{"service":"中型搬家","checked": false},
{"service":"專業搬家","checked": false},
{"service":"大型搬家","checked": false},
{"service":"可運單人床墊","checked": false},
{"service":"可運雙人床墊","checked": false},
{"service":"可提供床墊套","checked": false},
{"service":"IKEA傢俱代運","checked": false},
{"service":"IKEA傢俱代買代運","checked": false},
];

//knockoutjs start
function serviceModel(service) {
    this.service = ko.observable(service);
    this.checked = ko.observable();
}
function AppViewModel() {
    var self = this
    self.services = ko.observableArray([]);
    self.removePerson = function() {
        self.services.remove(this);
    }
    self.checkedlist = "";
    self.getAllChecked = ko.computed(function(){
    	self.checkedlist = "";
        ko.utils.arrayForEach(self.services(), function(item){
            //console.log(item.service() + " " + item.checked());

            if(item.checked()) {
                if(self.checkedlist == "") {
                    self.checkedlist = item.service();
                } else {
        	       self.checkedlist =  item.service() + "," + self.checkedlist;
               }
            }
            //console.log("selectedDistrict " + item.value + " " + self.selectedDistrictValue());
            //return item.value === self.selectedDistrictValue();
        });
        return self.checkedlist;
    });
}

var appvm_services = new AppViewModel()
/*
var arrservices = [
"兼職搬家",
"自助搬家",
"套房搬家",
"小型搬家",
"有搬物服務",
"中型搬家",
"專業搬家",
"大型搬家",
"可運單人床墊",
"可運雙人床墊",
"可提供床墊套",
"IKEA傢俱代運",
"IKEA傢俱代買代運",
];
*/
var arrservices = [
"兼職搬家",
"專業搬家",
"自助搬家",
"有搬物服務",
"可提供床墊套",
"IKEA傢俱代運",
];
/*
for debug
var checkedmap ={};
checkedmap["兼職搬家"] = true;
checkedmap["自助搬家"] = true;
*/
arrservices.forEach(function(entry) {
	appvm_services.services.push(new serviceModel(entry));
});

ko.applyBindings(appvm_services, document.getElementById("services"));

