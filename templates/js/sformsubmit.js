oFormObject = document.forms["myform"];
var done = [false, false, false];

function mycallback(obj) {
	console.log("obj:");
	console.log(obj);
	var location = obj.location;
	var num = obj.num;
	var lat, lng;
	if(location == null){
		lat = lng = 0;
	} else {
		lat = location.lat();
		lng = location.lng();
	}
	var latmap = ["lat", "lat_from", "lat_to"]; 
	var lngmap = ["lng", "lng_from", "lng_to"];
	if(num>=1 && num<=3) {
		oFormObject.elements[latmap[num-1]].value = lat;
		oFormObject.elements[lngmap[num-1]].value = lng;
		done[num-1] = true;
	}
	if(done[0]==true&&done[1]==true&&done[2]==true) {
		oFormObject.submit();
	}
}
function sformsubmit() {
	address1 = document.getElementById("city").value + 
			document.getElementById("district").value +
			document.getElementById("roadname").value;
	address2 = document.getElementById("county_from").value + 
			document.getElementById("district_from").value +
			document.getElementById("roadname_from").value;
	address3 = document.getElementById("county_to").value + 
			document.getElementById("district_to").value +
			document.getElementById("roadname_to").value;
	codeAddress(address1, 1, mycallback);
	codeAddress(address2, 2, mycallback);
	codeAddress(address3, 3, mycallback);
}

function codeAddress(address, num, callback) 
{
    //console.log('codeAddress()');
	geocoder = new google.maps.Geocoder();
	geocoder.geocode( {address:address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) 
		{
			console.log('Got it.');
			console.log(results[0].formatted_address);

			//map.setCenter(results[0].geometry.location);//center the map over the result
			//place a marker at the location
			console.log(results[0].geometry.location.toString());
			
            if( typeof callback == 'function' ) {
            	callback( {num:num,location:results[0].geometry.location});
            }
			// submit of form here.
			
		} else {
			//console.log('NO');
			callback( {num:num, location:null});
			console.log('Geocode was not successful for the following reason: ' + status);
		}
	});
}
