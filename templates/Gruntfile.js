module.exports = function(grunt) {

  // 專案設定
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      my_target: {
        files: [{
            expand: true,
            cwd: 'js',
            src: '*.js',
            dest: 'dist/js/'
        }]
      }
    }
  });

  // 載入可以提供 uglify task 的 plugin
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // 預設的 task
  grunt.registerTask('default', ['uglify']);

};
