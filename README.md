Go語言在GAE上操作CRUD基本範例 
--------------------------------------------------
GAE(Google App Engine)平台上使用Go語言操作CRUD的範例程式碼, 資料庫使用GAE的Datastore。

##前言

GAE是「Google應用服務引擎」在Google的平台上執行您的網路應用程式, 有**免費**的配額可以使用, 起初僅支援Python、JAVA, 後來才加入了Go、PHP等。

## Getting Started

1. 申請 [Google App Engine](http://appengine.google.com), Create Application, 請記下您的**Application Identifier**。

2. 安裝Go Tools: [http://golang.org/doc/install](http://golang.org/doc/install)。

3. 安裝[App Engine SDK](https://developers.google.com/appengine/docs/go/gettingstarted/devenvironment)。

4. Clone:
	
		git clone git@github.com:yfxie/go-lang-crud-on-gae.git
		cd go-lang-crud-on-gae/

	修改app.yaml檔案, 將 _Application Identifier_ 填入Step1所設的名稱

		application: helloworld
		version: 1
		runtime: go
		api_version: go1

		handlers:
		- url: /.*
		  script: _go_app

5. Local Testing: 
		
		dev_appserver.py .

	Server: http://localhost:8080

	AdminServer: http://localhost:8000

6. Deploy to GAE:

		appcfg.py update .

	Your app will running at _http://yourname.appspot.com_
