package golang_gae_crud

import (
        "net/http"


	"appengine"
	"appengine/datastore"
	"appengine/user"
	//"google.golang.org/appengine/log"
	"fmt"
	"html/template"
	//"strconv"
	"time"
	"strings"
	"encoding/json"
	"strconv"
)

// 存入 Datastore 資料形態
type ServiceSeller struct {
	Id      int64
	Owner string
	Nickname string
	Username  string
	Telephone string
	Line string
	Email string
	City string
	District string
	Roadname string
	Checkstrs string
	Imgurls string
	Radiocar string
	Content string
	Toggleonoff bool
	Lat float64
	Lng float64
	Lat_from float64
	Lng_from float64
	Lat_to float64
	Lng_to float64

	//turn round
	TRToggleonoff bool
	County_from string
	District_from string
	Roadname_from string
	County_to string
	District_to string
	Roadname_to string
	TRDate   string

	Date    time.Time
}

// 存入 Datastore 資料形態
type ServiceBuyer struct {
	Id      int64
	Owner string
	Username  string
	Email string
	County_from string
	District_from string
	Roadname_from string
	County_to string
	District_to string
	Roadname_to string
	ServiceRequired string
	Imgurls string
	Content string
	Toggleonoff bool

	Lat_from float64
	Lng_from float64
	Lat_to float64
	Lng_to float64

	StartDate   string
	EndDate   string
	Date    time.Time
}



// template 無內建判斷等值的func, 須要手動實作
var templateFuncMap = template.FuncMap{"eq": func(a, b string) bool { return a == b }, 
										"divide": func(a, b int) int { return a / b },
										"mod": func(a, b int) int { return a % b },
										"add": func(a, b int) int { return a + b },
									}

func render(w http.ResponseWriter, filename string, context interface{}) error {
	var temp = template.New(filename)
	return template.Must(temp.Funcs(templateFuncMap).ParseFiles("templates/"+filename)).Execute(w, context)
}

type HandleFuncType func(http.ResponseWriter, *http.Request)
type HandleFuncUserCheckType func(http.ResponseWriter, *http.Request, appengine.Context, *user.User)
type HandleFuncCRUDType func(http.ResponseWriter, *http.Request, appengine.Context, *user.User, string, string)

func usercheck_decorator(handlefunc HandleFuncUserCheckType) HandleFuncType {
	outfunc := func(w http.ResponseWriter, r *http.Request) {
		/*
		c := appengine.NewContext(r)
		u := user.Current(c)
		if u == nil {
			// log in fail
			//fmt.Fprint(w, "permission denied")
        	http.Redirect(w, r, "/notfound", http.StatusFound)

			return
		}
	*/

		// fore to login
		c := appengine.NewContext(r)
		u := user.Current(c)
		if u == nil {
			url, err := user.LoginURL(c, r.URL.String())
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.Header().Set("Location", url)
			w.WriteHeader(http.StatusFound)
			return
		}
		handlefunc(w, r, c, u)
	}

	return outfunc
}

func crud_object_decorator(handlefunc HandleFuncCRUDType, pkn string, entity_name string) HandleFuncUserCheckType {
	outfunc := func(w http.ResponseWriter, r *http.Request, c appengine.Context, u *user.User) {

		handlefunc(w, r, c, u, pkn, entity_name)
	}
	return outfunc
}

func init() {
	router()
}

func router() {
	http.HandleFunc("/notfound", notfound)
	http.HandleFunc("/", index)

	http.HandleFunc("/maps", jsonapi)
	http.HandleFunc("/latest", apilatest)
	http.HandleFunc("/latestquery", apilatestquery)
	http.HandleFunc("/latesttr", apilatesttr)
	http.HandleFunc("/search", apisearch)

	http.HandleFunc("/sform", usercheck_decorator(seller_read))
	http.HandleFunc("/screate", usercheck_decorator(crud_object_decorator(seller_create_or_update, "Sellers", "ServiceSeller")))

	http.HandleFunc("/cform", usercheck_decorator(buyer_read))
	http.HandleFunc("/ccreate", usercheck_decorator(crud_object_decorator(buyer_create_or_update, "Buyers", "ServiceBuyer")))

    http.HandleFunc("/logout", logout)

}
type WebWrapperEntry struct {
    Lat float64
    Lng float64
}
//var data = []byte(`[{"photo_id": 27932, "photo_title": "Atardecer en Embalse"}`]
func jsonapi(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

    w.Header().Set("Content-Type", "application/json")
/*
    err := json.Unmarshal(data, &Gps_db)
    if err != nil {
        log.Fatalln("error:", err)
    }
*/
    //fmt.Fprintf(w, , )


    // get all marker from database
	q := datastore.NewQuery("ServiceSeller").Filter("Toggleonoff =", true).Limit(100)
	ServiceSellers := make([]ServiceSeller, 0, 100)
	_, err := q.GetAll(c, &ServiceSellers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
    //var entries []WebWrapperEntry
    //entries = append(entries, WebWrapperEntry{1.0,1.0})
    //entries = append(entries, WebWrapperEntry{2.0,2.0})
	w.Header().Set("Cache-Control", "public, max-age=60")
    json.NewEncoder(w).Encode(ServiceSellers)
}
//hostname := r.URL.Query().Get("hostname")
func apisearch(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	county := r.URL.Query().Get("county")
	district := r.URL.Query().Get("district")
	ckservice := r.URL.Query().Get("ckservice")
	arrckservice := strings.Split(ckservice, ",")

	c.Infof("county:%v", county)
	c.Infof("district:%v", district)
	c.Infof("ckservice:%v", ckservice)
	c.Infof("arrckservice:%v", arrckservice)

    w.Header().Set("Content-Type", "application/json")
/*
    // get all marker from database
type ServiceSeller struct {
	Id      int64
	Owner string
	Nickname string
	Username  string
	Telephone string
	Line string
	Email string
	City string
	District string
	Roadname string
	Checkstrs string
	Imgurls string
	Radiocar string
	Content string
	Toggleonoff bool
	Lat float64
	Lng float64
	Date    time.Time
}
*/
	q := datastore.NewQuery("ServiceSeller")
	if(county != "") {
		q = q.Filter("City =", county)
	}
	if(district != "") {
		q = q.Filter("District =",district)
	}
/*
	for _, service := range arrckservice {
		if(service != "") {
			c.Infof("service:%v", service)
			q = q.Filter("Checkstrs <", service)
		}
	}
*/	
	q = q.Order("-Date").Limit(100)


	ServiceSellers := make([]ServiceSeller, 0, 100)
	_, err := q.GetAll(c, &ServiceSellers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// workaround, full-text search..GG
	for _, service := range arrckservice {
		if(service != "") {
			c.Infof("service:%v", service)
			ServiceSellers_next := make([]ServiceSeller, 0, 0)

			for _, seller := range ServiceSellers {
				if(strings.Contains(seller.Checkstrs, service) == true) {
					ServiceSellers_next = append(ServiceSellers_next, seller)
				}
			}
			ServiceSellers = ServiceSellers_next
			c.Infof("ServiceSellers:%v", ServiceSellers)
		}
	}
	w.Header().Set("Cache-Control", "public, max-age=60")


	//c.Infof("QQQQQQQQ:%v", len(foundkeys))
    json.NewEncoder(w).Encode(ServiceSellers)
}
func apilatest(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
    w.Header().Set("Content-Type", "application/json")
    // get all marker from database
	q := datastore.NewQuery("ServiceSeller").Filter("Toggleonoff =", true).Order("-Date").Limit(100)
	ServiceSellers := make([]ServiceSeller, 0, 100)
	_, err := q.GetAll(c, &ServiceSellers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Cache-Control", "public, max-age=60")
    json.NewEncoder(w).Encode(ServiceSellers)
}

func apilatestquery(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
    w.Header().Set("Content-Type", "application/json")
    // get all marker from database
	q := datastore.NewQuery("ServiceBuyer").Filter("Toggleonoff =", true).Order("-Date").Limit(100)
	ServiceBuyers := make([]ServiceBuyer, 0, 100)
	_, err := q.GetAll(c, &ServiceBuyers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Cache-Control", "public, max-age=60")
    json.NewEncoder(w).Encode(ServiceBuyers)
}

func apilatesttr(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
    w.Header().Set("Content-Type", "application/json")
    // get all marker from database
	q := datastore.NewQuery("ServiceSeller").Filter("TRToggleonoff =", true).Order("-Date").Limit(100)
	ServiceSeller := make([]ServiceSeller, 0, 100)
	_, err := q.GetAll(c, &ServiceSeller)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Cache-Control", "public, max-age=60")
    json.NewEncoder(w).Encode(ServiceSeller)
}



// first read(newquery -> get)
func seller_read(w http.ResponseWriter, r *http.Request, c appengine.Context, current_user *user.User) {

//	q := datastore.NewQuery(entity_name).Ancestor(getParentKey(c, pkn)).Order("-Date").Limit(10)
	q := datastore.NewQuery("ServiceSeller").
	                Filter("Owner =", current_user.String()).Limit(1)
	ServiceSellers := make([]ServiceSeller, 0, 1)
	keys, err := q.GetAll(c, &ServiceSellers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// fullfill the Id
	if len(keys) > 0 {
		ServiceSellers[0].Id = keys[0].IntID()
	}

	//Rendering start, SformInterface is any name, but Ckservice and ServiceSeller is important name
type SSformInterface struct {
	Ckservice []string
	Ckimgurls []string
	ServiceSeller *ServiceSeller
	CurrentUser string
}
	//defaultck := []string{"自助搬家","IKEA傢俱運送"}

	con := SSformInterface{nil, nil, nil, current_user.String()}
	if len(keys) > 0 {
		con.ServiceSeller = &ServiceSellers[0]
		if ServiceSellers[0].Checkstrs != ""{
			con.Ckservice = strings.Split(ServiceSellers[0].Checkstrs, ",")
		}
		if ServiceSellers[0].Imgurls != ""{
			con.Ckimgurls = strings.Split(ServiceSellers[0].Imgurls, ",")
		}
	}

	var temp = template.New("sform.htm")
	template.Must(temp.Funcs(templateFuncMap).ParseFiles("templates/sform.htm")).Execute(w, con)

	//Rendering end
}

// first read(newquery -> get), secand insert(newkey -> put)
func seller_create_or_update(w http.ResponseWriter, r *http.Request, c appengine.Context, current_user *user.User, pkn string, entity_name string) {
/*
type ServiceSeller struct {
	Id      int64
	Owner string
	Nickname string
	Username  string
	Telephone string
	Line string
	Email string
	City string
	District string
	Roadname string
	Checkstrs string
	Imgurls string
	Radiocar string
	Content string
	Toggleonoff bool
	Lat float64
	Lng float64
	Lat_from float64
	Lng_from float64
	Lat_to float64
	Lng_to float64

	//turn round
	TRToggleonoff bool
	County_from string
	District_from string
	Roadname_from string
	County_to string
	District_to string
	Roadname_to string
	Date    time.Time
}
*/

	entity := ServiceSeller{
		Owner:  current_user.String(),		//gamil account
		Toggleonoff: r.FormValue("toggleonoff") == "on",
		Nickname: r.FormValue("nickname"),
		Username: r.FormValue("username"),
		Telephone: r.FormValue("telephone"),
		Line: r.FormValue("line"),
		Email: r.FormValue("email"),
		City: r.FormValue("city"),
		District: r.FormValue("district"),
		Roadname: r.FormValue("roadname"),
		// radio handling
		Radiocar: r.FormValue("radiocar"),
		// checkbox handling
		Checkstrs: strings.Join(r.Form["ckservice"][:],","),
		Imgurls: strings.Join(r.Form["ckimgurl"][:],","),
		Content: r.FormValue("content"),
		Date:    time.Now(),
		TRToggleonoff:  r.FormValue("trtoggleonoff") == "on",
		County_from: r.FormValue("county_from"),
		District_from: r.FormValue("district_from"),
		Roadname_from: r.FormValue("roadname_from"),
		County_to: r.FormValue("county_to"),
		District_to: r.FormValue("district_to"),
		Roadname_to: r.FormValue("roadname_to"),
		TRDate: r.FormValue("trdate"),
	}
    f, err := strconv.ParseFloat(r.FormValue("lat"), 64)
    if err == nil {
    	entity.Lat = f
    }
    f, err = strconv.ParseFloat(r.FormValue("lng"), 64)
    if err == nil {
    	entity.Lng = f
    }
    f, err = strconv.ParseFloat(r.FormValue("lat_from"), 64)
    if err == nil {
    	entity.Lat_from = f
    }
    f, err = strconv.ParseFloat(r.FormValue("lng_from"), 64)
    if err == nil {
    	entity.Lng_from = f
    }
    f, err = strconv.ParseFloat(r.FormValue("lat_to"), 64)
    if err == nil {
    	entity.Lat_to = f
    }
    f, err = strconv.ParseFloat(r.FormValue("lng_to"), 64)
    if err == nil {
    	entity.Lng_to = f
    }

    // Debug        
	//c.Infof("%v", r.Form["ckservice"])

	q := datastore.NewQuery("ServiceSeller").
	                Filter("Owner =", current_user.String()).
	                Limit(1)

	ServiceSellers := make([]ServiceSeller, 0, 1)

	foundkeys, err := q.GetAll(c, &ServiceSellers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(foundkeys) > 0 {
		// Found, we insert data by foundkey
		if _, err := datastore.Put(c, foundkeys[0], &entity); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		// Not Found, we should firstly create new key for inserting new data
		//func NewIncompleteKey(c context.Context, kind string, parent *Key) *Key
		//NewIncompleteKey creates a new incomplete key. kind cannot be empty.
		// New Item
		newkey := datastore.NewIncompleteKey(c, entity_name, getParentKey(c, pkn))
		//insert->Put, Read->Get
		if _, err := datastore.Put(c, newkey, &entity); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	// Redirect to /
	http.Redirect(w, r, "/html/index.html", http.StatusFound)
}
// first read(newquery -> get)
func buyer_read(w http.ResponseWriter, r *http.Request, c appengine.Context, current_user *user.User) {

//	q := datastore.NewQuery(entity_name).Ancestor(getParentKey(c, pkn)).Order("-Date").Limit(10)
	q := datastore.NewQuery("ServiceBuyer").
	                Filter("Owner =", current_user.String()).Limit(1)
	ServiceBuyers := make([]ServiceBuyer, 0, 1)
	keys, err := q.GetAll(c, &ServiceBuyers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// fullfill the Id
	if len(keys) > 0 {
		ServiceBuyers[0].Id = keys[0].IntID()
	}

	//Rendering start, SformInterface is any name, but Ckservice and ServiceBuyer is important name
type WebformInterface struct {
	Imgurls []string
	ServiceBuyer *ServiceBuyer
	CurrentUser string
}

	inter := WebformInterface{nil, nil, current_user.String()}
	if len(keys) > 0 {
		inter.ServiceBuyer = &ServiceBuyers[0]
		if ServiceBuyers[0].Imgurls != ""{
			inter.Imgurls = strings.Split(ServiceBuyers[0].Imgurls, ",")
		}
	}

	var temp = template.New("cform.htm")
	template.Must(temp.Funcs(templateFuncMap).ParseFiles("templates/cform.htm")).Execute(w, inter)
}

// first read(newquery -> get), secand insert(newkey -> put)
func buyer_create_or_update(w http.ResponseWriter, r *http.Request, c appengine.Context, current_user *user.User, pkn string, entity_name string) {
/*
type ServiceBuyer struct {
	Id      int64
	Owner string
	Username  string
	Email string
	County_from string
	District_from string
	Roadname_from string
	County_to string
	District_to string
	Roadname_to string
	ServiceRequired string
	Imgurls string
	Content string
	Toggleonoff bool
	Lat float64
	Lng float64
	StartDate   string
	EndDate   string
	Date    time.Time
}
*/
	entity := ServiceBuyer{
		Owner:  current_user.String(),		//gamil account
		Toggleonoff: r.FormValue("toggleonoff") == "on",
		Username: r.FormValue("username"),
		Email: r.FormValue("email"),
		County_from: r.FormValue("county_from"),
		District_from: r.FormValue("district_from"),
		Roadname_from: r.FormValue("roadname_from"),
		County_to: r.FormValue("county_to"),
		District_to: r.FormValue("district_to"),
		Roadname_to: r.FormValue("roadname_to"),
		Imgurls: strings.Join(r.Form["ckimgurl"][:],","),
		Content: r.FormValue("content"),
		StartDate: r.FormValue("startdate"),
		EndDate: r.FormValue("enddate"),
		Date:    time.Now(),
	}

    f, err := strconv.ParseFloat(r.FormValue("lat_from"), 64)
    if err == nil {
    	entity.Lat_from = f
    }
    f, err = strconv.ParseFloat(r.FormValue("lng_from"), 64)
    if err == nil {
    	entity.Lng_from = f
    }
    f, err = strconv.ParseFloat(r.FormValue("lat_to"), 64)
    if err == nil {
    	entity.Lat_to = f
    }
    f, err = strconv.ParseFloat(r.FormValue("lng_to"), 64)
    if err == nil {
    	entity.Lng_to = f
    }
    // Debug        
	//c.Infof("%v", r.Form["ckservice"])

	q := datastore.NewQuery("ServiceBuyer").
	                Filter("Owner =", current_user.String()).
	                Limit(1)

	ServiceBuyers := make([]ServiceBuyer, 0, 1)

	foundkeys, err := q.GetAll(c, &ServiceBuyers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(foundkeys) > 0 {
		// Found, we insert data by foundkey
		if _, err := datastore.Put(c, foundkeys[0], &entity); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		// Not Found, we should firstly create new key for inserting new data
		//func NewIncompleteKey(c context.Context, kind string, parent *Key) *Key
		//NewIncompleteKey creates a new incomplete key. kind cannot be empty.
		// New Item
		newkey := datastore.NewIncompleteKey(c, entity_name, getParentKey(c, pkn))
		//insert->Put, Read->Get
		if _, err := datastore.Put(c, newkey, &entity); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	// Redirect to /
	http.Redirect(w, r, "/html/index.html", http.StatusFound)
}

func index(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/html/index.html", http.StatusFound)
}

func logout(w http.ResponseWriter, r *http.Request) {
    c := appengine.NewContext(r)
    url, err := user.LogoutURL(c, "/html/index.html")
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    w.Header().Set("Location", url)
    w.WriteHeader(http.StatusFound)
}

func notfound(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "not found")

}

// getParentKey returns the key used for all Sellers entries.
func getParentKey(c appengine.Context, parent_kindname string) *datastore.Key {
        // The string "default_Sellers" here could be varied to have multiple Sellerss.
        // Entities are the unit of storage and are associated with a key.
        //A key consists of an optional parent key, a string application ID, a string kind (also known as an entity type), and either a StringID or an IntID. A StringID is also known as an entity name or key name.
        //The Get and Put functions load and save an entity's contents. An entity's contents are typically represented by a struct pointer.
        //func NewKey(c context.Context, kind, stringID string, intID int64, parent *Key) *Key
        return datastore.NewKey(c, parent_kindname, "default_" +  parent_kindname, 0, nil)
}
